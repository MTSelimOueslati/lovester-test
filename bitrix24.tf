# Configure the AWS provider
provider "aws" {
  region = "us-east-1"
}

# Provision an EC2 instance
resource "aws_instance" "bitrix24" {
  ami           = "ami-0c94855ba95c71c99"
  instance_type = "t2.micro"
  key_name      = "your_key"
  vpc_security_group_ids = [aws_security_group.bitrix24.id]
  user_data     = <<-EOF
                  #!/bin/bash
                  sudo yum update -y
                  sudo yum install docker -y
                  sudo service docker start
                  sudo docker pull 1c/bitrix-env:7.4
                  sudo docker run -p 80:80 -p 443:443 -d --name bitrix24 1c/bitrix-env:7.4
                  EOF
}

# Configure the security group for the EC2 instance
resource "aws_security_group" "bitrix24" {
  name        = "bitrix24"
  description = "Allow inbound HTTP and HTTPS traffic"
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Output the public IP address of the EC2 instance
output "bitrix24_public_ip" {
  value = aws_instance.bitrix24.public_ip
}
