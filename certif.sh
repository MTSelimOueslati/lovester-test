# Configure the web server
sudo apt-get update
sudo apt-get install -y \
    nginx

# Generate a self-signed certificate
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 \
    -keyout /etc/nginx/ssl/server.key \
    -out /etc/nginx/ssl/server.crt \
    -subj "/C=US/ST=CA/L=San Francisco/O=MyOrg/OU=MyDept/CN=mycrm.com"

# Configure Nginx to use the self-signed certificate
sudo tee /etc/nginx/sites-available/mycrm <<EOF
server {
    listen 443 ssl;
    server_name mycrm.com;
    ssl_certificate /etc/nginx/ssl/server.crt;
    ssl_certificate_key /etc/nginx/ssl/server.key;

    location / {
        proxy_pass http://localhost:8080;
        proxy_set_header Host \$host;
        proxy_set_header X-Real-IP \$remote_addr;
        proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto \$scheme;
    }
}
EOF

# Enable the Nginx virtual host
sudo ln -s /etc/nginx/sites-available/mycrm /etc/nginx/sites-enabled/

# Remove the default Nginx virtual host
sudo rm /etc/nginx/sites-enabled/default

# Restart Nginx
sudo systemctl restart nginx
