sudo apt-get update
sudo apt-get install -y \
    nginx \
    certbot \
    python3-certbot-nginx

# Configure SSL
sudo certbot --nginx -d example.com
sudo systemctl reload nginx
