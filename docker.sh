sudo apt-get update
sudo apt-get install -y \
    docker-ce \
    docker-ce-cli \
    containerd.io

# Start Docker service
sudo systemctl start docker
sudo systemctl enable docker
